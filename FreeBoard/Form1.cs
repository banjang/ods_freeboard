﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FreeBoard
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// ctor
        /// </summary>
        public Form1() { InitializeComponent(); }

        /// <summary>
        /// properties
        /// </summary>
        public Color[] colors = new Color[] { Color.White, Color.Yellow, Color.Red, Color.FromArgb(56, 109, 71) };
        public Pen[] drawPens;
        public float currentPenWidth;
        public int penindex;
        public bool isDrawing = false;
        public Point lastpoint = new Point();        
        public List<Bitmap> saveimages;
        public int bitmaplength = 25;

        /// <summary>
        /// load
        /// </summary>        
        private void Form1_Load(object sender, EventArgs e)
        {            
            Bitmap bit = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            using (var g = Graphics.FromImage(bit))
            {
                Brush br = new SolidBrush(colors[3]);
                g.FillRectangle(br,new Rectangle(0,0,bit.Width, bit.Height));
            }                
            pictureBox1.Image = bit;
            drawPens = new Pen[] { new Pen(colors[0]), new Pen(colors[1]), new Pen(colors[2]), new Pen(colors[3]) };
            penindex = 0;
            currentPenWidth = 3.0f;
            for(int i=0;i<drawPens.Length-1;i++)            
            { 
                drawPens[i].Width = currentPenWidth;                
                drawPens[i].DashCap = System.Drawing.Drawing2D.DashCap.Round;                
            }            
            //erase by 10 times
            drawPens[drawPens.Length-1].Width = currentPenWidth * 10;
            drawPens[drawPens.Length-1].StartCap = System.Drawing.Drawing2D.LineCap.Round;
            drawPens[drawPens.Length-1].EndCap = System.Drawing.Drawing2D.LineCap.Round;
            //mousewheel event
            this.MouseWheel += new MouseEventHandler(BaseForm_MouseWheel);
            //data
            saveimages = new List<Bitmap>();
            lastpoint = new Point();
            this.Text = string.Format("FreeBoard    [Pensize : {0}]", Convert.ToString(currentPenWidth));
        }

        /// <summary>
        /// shown
        /// </summary>        
        private void Form1_Shown(object sender, EventArgs e)
        {
            Bitmap img = new Bitmap(pictureBox1.Image);            //save first img
            PushImage(img);
            panel1.BackColor = colors[penindex];
        }

        /// <summary>
        /// resiz event
        /// </summary>        
        private void Form1_Resize(object sender, EventArgs e)
        {
            reDrawImage(getLastImage());
        }

        /// <summary>
        /// push lastimg
        /// </summary>        
        public void PushImage(Bitmap item)
        {
            saveimages.Add(item);
            if (saveimages.Count > bitmaplength)
            {
                saveimages.RemoveAt(0);
            }
        }

        /// <summary>
        /// pop lastImage
        /// </summary>        
        public Bitmap PopIamge()
        {
            Bitmap img = saveimages[saveimages.Count-1];
            if (saveimages.Count > 1)
            {
                saveimages.RemoveAt(saveimages.Count - 1);
            }            
            return img;
        }

        /// <summary>
        /// get lastImage
        /// </summary>        
        public Bitmap getLastImage()
        {
            return saveimages[saveimages.Count-1];
        }

        /// <summary>
        /// redrawlastimg
        /// </summary>
        public void reDrawImage(Bitmap img)
        {
            Bitmap bit = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            using (var g = Graphics.FromImage(bit))
            {
                Brush br = new SolidBrush(colors[3]);
                g.FillRectangle(br, new Rectangle(0, 0, bit.Width, bit.Height));
                g.DrawImage(img, new Point(0, 0));
            }
            pictureBox1.Image = bit;
        }

        /// <summary>
        /// key down
        /// </summary>        
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F11)  //full screen toggle
            {
                if (this.FormBorderStyle == FormBorderStyle.Sizable)
                {
                    this.TopMost = true;
                    this.FormBorderStyle = FormBorderStyle.None;
                    this.WindowState = FormWindowState.Maximized;
                }
                else
                {
                    this.TopMost = false;
                    this.FormBorderStyle = FormBorderStyle.Sizable;
                    this.WindowState = FormWindowState.Normal;
                }
                reDrawImage(getLastImage());
            }
            else if (e.KeyCode == Keys.Space)
            {                
                reDrawImage(PopIamge());
            }
            else if (e.KeyCode == Keys.Escape)
            {
                //clear
                Bitmap bit = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                using (var g = Graphics.FromImage(bit))
                {
                    Brush br = new SolidBrush(colors[3]);
                    g.FillRectangle(br, new Rectangle(0, 0, bit.Width, bit.Height));                    
                }
                pictureBox1.Image = bit;
                //clear before image
                saveimages.Clear();                
                //reset first image
                Bitmap img = new Bitmap(pictureBox1.Image);
                PushImage(img);
            }
        }

        /// <summary>
        /// mouse move draw
        /// </summary>        
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {            
            if(e.Button == MouseButtons.Left)
            {
                if (this.isDrawing)
                {
                    using (Graphics g = Graphics.FromImage(pictureBox1.Image))
                    {
                        //smooth
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        // Draw a line.
                        g.DrawLine(drawPens[penindex], lastpoint, e.Location);                        
                        //push last location
                        lastpoint = e.Location;
                    }
                    pictureBox1.Invalidate();
                }                
            }
        }
                
        /// <summary>
        /// mouse down begin drawing
        /// </summary>        
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)      //start drawing
            {
                this.isDrawing = true;
                lastpoint.X = e.X;
                lastpoint.Y = e.Y;
            } else if (e.Button == MouseButtons.Right) {        //change pen color
                penindex = (penindex + 1) % drawPens.Length;
                panel1.BackColor = colors[penindex];
            }
        }

        /// <summary>
        /// mouseup save last image
        /// </summary>        
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.isDrawing = false;
                Bitmap img = new Bitmap(pictureBox1.Image);            //bitmap copy            
                PushImage(img);    
            }
        }

        /// <summary>
        /// leave = draw false
        /// </summary>        
        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            this.isDrawing = false;
        }

        /// <summary>
        /// mouse wheel event
        /// </summary>        
        private void BaseForm_MouseWheel(object sender, MouseEventArgs e)
        {   
            if (e.Delta < 0)
            {
                currentPenWidth = currentPenWidth - 1.0f;
                if (currentPenWidth < 1.0f) { currentPenWidth = 1.0f;  }
                for (int i = 0; i < drawPens.Length - 1; i++)
                {
                    drawPens[i].Width = currentPenWidth;                    
                }
                //erase by 10 times
                drawPens[drawPens.Length - 1].Width = currentPenWidth * 10;
            } else if(e.Delta > 0) {
                currentPenWidth = currentPenWidth + 1.0f;
                if (currentPenWidth > 100.0f) { currentPenWidth = 100.0f; }
                for (int i = 0; i < drawPens.Length - 1; i++)
                {
                    drawPens[i].Width = currentPenWidth;
                }
                //erase by 10 times
                drawPens[drawPens.Length - 1].Width = currentPenWidth * 10;
            }
            this.Text = string.Format("FreeBoard    [Pensize : {0}]", Convert.ToString(currentPenWidth));
        }
    }
}
